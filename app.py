from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello():
    print('root called')
#    name = "Hello World"
#    return name
    return "<a href='/good'>Run Good!</a>"

@app.route('/good')
def good():
    print('good called')
    name = "Good"
    return name

if __name__ == "__main__":
    print('before')
    app.run(debug=True, port=3000)
